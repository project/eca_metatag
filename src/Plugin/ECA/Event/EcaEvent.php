<?php

namespace Drupal\eca_metatag\Plugin\ECA\Event;

use Drupal\eca\Plugin\ECA\Event\EventBase;
use Drupal\eca_metatag\EcaEvents;
use Drupal\eca_metatag\Event\AlterEvent;
use Drupal\eca_metatag\Event\TagsEvent;

/**
 * Plugin implementation of the ECA Events for eca_metatag.
 *
 * @EcaEvent(
 *   id = "eca_metatag",
 *   deriver = "Drupal\eca_metatag\Plugin\ECA\Event\EcaEventDeriver",
 *   eca_version_introduced = "1.0.0"
 * )
 */
class EcaEvent extends EventBase {

  /**
   * {@inheritdoc}
   */
  public static function definitions(): array {
    $definitions = [];
    $definitions['tags'] = [
      'label' => 'Provide a list of tags',
      'event_name' => EcaEvents::TAGS,
      'event_class' => TagsEvent::class,
    ];
    $definitions['alter'] = [
      'label' => 'Alter metatags',
      'event_name' => EcaEvents::ALTER,
      'event_class' => AlterEvent::class,
    ];
    return $definitions;
  }

}
