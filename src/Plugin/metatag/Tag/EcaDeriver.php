<?php

namespace Drupal\eca_metatag\Plugin\metatag\Tag;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\eca_metatag\EcaEvents;
use Drupal\eca_metatag\Event\TagsEvent;

/**
 * Derive metatag plugins from an ECA event.
 */
class EcaDeriver extends DeriverBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $this->derivatives = [];

    $event = new TagsEvent();
    // @phpstan-ignore-next-line
    \Drupal::service('event_dispatcher')->dispatch($event, EcaEvents::TAGS);

    foreach ($event->getTags() as $name => $definition) {
      $this->derivatives[$name] = [
        'id' => 'eca:' . $name,
        'label' => $this->t('@label', ['@label' => $definition['label']]),
        'name' => $name,
        'description' => $definition['description'],
      ] + $base_plugin_definition;
    }
    return $this->derivatives;
  }

}
