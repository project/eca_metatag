<?php

namespace Drupal\eca_metatag\Plugin\metatag\Tag;

use Drupal\metatag\Plugin\metatag\Tag\MetaNameBase;

/**
 * The standard page title.
 *
 * @MetatagTag(
 *   id = "eca",
 *   group = "basic",
 *   type = "label",
 *   weight = 0,
 *   secure = FALSE,
 *   multiple = FALSE,
 *   trimmable = TRUE,
 *   deriver = "Drupal\eca_metatag\Plugin\metatag\Tag\EcaDeriver"
 * )
 */
class Eca extends MetaNameBase {

}
