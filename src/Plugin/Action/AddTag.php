<?php

namespace Drupal\eca_metatag\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\eca_metatag\Event\TagsEvent;

/**
 * Action to add a new metatag.
 *
 * This only makes sense after a \Drupal\eca_metatag\Event\TagsEvent.
 *
 * @Action(
 *   id = "eca_metatag_add_tag",
 *   label = @Translation("Add tag"),
 *   eca_version_introduced = "1.0.0"
 * )
 */
class AddTag extends ConfigurableActionBase {

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = AccessResult::forbidden();
    if (isset($this->event) && ($this->event instanceof TagsEvent)) {
      $result = AccessResult::allowed();
    }
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    if (isset($this->event) && ($this->event instanceof TagsEvent)) {
      $name = $this->tokenService->replace($this->configuration['name']);
      $label = $this->tokenService->replace($this->configuration['label']);
      $description = $this->tokenService->replace($this->configuration['description']);
      $this->event->addTag($name, $label, $description);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'name' => '',
      'label' => '',
      'description' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('Provide the name of the metatag for the head section of HTML pages.'),
      '#default_value' => $this->configuration['name'],
      '#required' => TRUE,
    ];
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('The label is used in the admin UI where the metatag can be configured.'),
      '#default_value' => $this->configuration['label'],
      '#required' => TRUE,
    ];
    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#description' => $this->t('The description for the field in the admin UI.'),
      '#default_value' => $this->configuration['description'],
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['name'] = $form_state->getValue('name');
    $this->configuration['label'] = $form_state->getValue('label');
    $this->configuration['description'] = $form_state->getValue('description');
    parent::submitConfigurationForm($form, $form_state);
  }

}
