<?php

namespace Drupal\eca_metatag\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\eca_metatag\Event\AlterEvent;

/**
 * Action to set any metatag's value.
 *
 * This only makes sense after a \Drupal\eca_metatag\Event\AlterEvent.
 *
 * @Action(
 *   id = "eca_metatag_set_tag_value",
 *   label = @Translation("Set tag value"),
 *   eca_version_introduced = "1.0.0"
 * )
 */
class SetTagValue extends ConfigurableActionBase {

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = AccessResult::forbidden();
    $name = $this->tokenService->replace($this->configuration['tag_name']);
    if (isset($this->event) && ($this->event instanceof AlterEvent) && $this->event->hasMetatag($name)) {
      $result = AccessResult::allowed();
    }
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    if (isset($this->event) && ($this->event instanceof AlterEvent)) {
      $name = $this->tokenService->replace($this->configuration['tag_name']);
      $value = $this->tokenService->replace($this->configuration['tag_value']);
      $this->event->setMetatagValue($name, $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'tag_name' => '',
      'tag_value' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['tag_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('Explain what a name is for.'),
      '#default_value' => $this->configuration['tag_name'],
      '#required' => TRUE,
    ];
    $form['tag_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#description' => $this->t('Explain where the label is used.'),
      '#default_value' => $this->configuration['tag_value'],
      '#required' => TRUE,
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['tag_name'] = $form_state->getValue('tag_name');
    $this->configuration['tag_value'] = $form_state->getValue('tag_value');
    parent::submitConfigurationForm($form, $form_state);
  }

}
