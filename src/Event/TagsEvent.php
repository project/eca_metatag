<?php

namespace Drupal\eca_metatag\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Provides an event for eca_metatag.
 *
 * @package Drupal\eca_metatag\Event
 */
class TagsEvent extends Event {

  /**
   * List of tag names.
   *
   * @var string[][]
   */
  protected array $tags = [];

  /**
   * Gets the list of tags.
   *
   * @return string[][]
   *   An array of tag names, labels, and descriptions.
   */
  public function getTags(): array {
    return $this->tags;
  }

  /**
   * Adds a tag to the list.
   *
   * @param string $name
   *   The name of the tag.
   * @param string $label
   *   The label of the tag.
   * @param string $description
   *   The description of the tag.
   */
  public function addTag(string $name, string $label, string $description): void {
    $this->tags[$name] = [
      'label' => $label,
      'description' => $description,
    ];
  }

}
