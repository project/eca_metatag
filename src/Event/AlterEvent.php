<?php

namespace Drupal\eca_metatag\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Provides an event for eca_metatag.
 *
 * @package Drupal\eca_metatag\Event
 */
class AlterEvent extends Event {

  /**
   * The list of existing metatag.
   *
   * @var array
   */
  protected array $metatags;

  /**
   * The context for metatags.
   *
   * @var array
   */
  protected array $context;

  /**
   * Constructor of the metatag alter event.
   *
   * @param array $metatags
   *   The list of existing metatag.
   * @param array $context
   *   The context for metatags.
   */
  public function __construct(array &$metatags, array &$context) {
    $this->metatags = &$metatags;
    $this->context = &$context;
  }

  /**
   * Determines if a metatag with the given name currently exists.
   *
   * @param string $name
   *   The name of the metatag.
   *
   * @return bool
   *   TRUE if a metatag by that name exists, FALSE otherwise.
   */
  public function hasMetatag(string $name): bool {
    return isset($this->metatags[$name]);
  }

  /**
   * Sets the named metatag to the given value, if it already exists.
   *
   * @param string $name
   *   The name of the metatag.
   * @param string $value
   *   The new value for that metatag.
   */
  public function setMetatagValue(string $name, string $value): void {
    if ($this->hasMetatag($name)) {
      $this->metatags[$name] = $value;
    }
  }

}
