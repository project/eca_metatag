<?php

namespace Drupal\eca_metatag;

/**
 * Defines events provided by the eca_metatag module.
 */
final class EcaEvents {

  /**
   * Event to collect a list of metatag plugins.
   *
   * @Event
   *
   * @var string
   */
  public const TAGS = 'eca_metatag.tags';

  /**
   * Event to alter metatags on a page.
   *
   * @Event
   *
   * @var string   */
  public const ALTER = 'eca_metatag.alter';

}
